=============================
Architektura
=============================

Diagram UML
------------------------------

.. uml::

    @startuml
    !define LIGHTGREEN
    !includeurl https://raw.githubusercontent.com/Drakemor/RedDress-PlantUML/master/style.puml


    class Customer{
    -id: Long
    -firstName: String
    -lastName: String
    +toString()
    +getters()
    }
    note left : this class is an entity

    class CustomerController{
    - CustomerRepositorycustomerRepository: CustomerRepository
    +getCustomers(): Iterable<Customer>
    +getUserById(customerId: Long): Customer
    }

    interface CustomerRepository {
    ~findByLastName(lastName: String): List<Customer>
    ~findById(id: Long): Customer
    }

    Customer --> CustomerRepository
    CustomerController --> CustomerRepository
    @enduml



Diagram przypadków użycia
-------------------------

.. uml::

    @startuml
    left to right direction

    actor "Klient" as k



    rectangle CustomerService {
        usecase "Otrzymaj informacje na temat wszystkich klientów" as UC1
        usecase "Otrzymaj informację na temat klienta o zadanym id" as UC2
    }
    k --> UC1
    k --> UC2


    @enduml

