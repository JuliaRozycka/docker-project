.. lab3 documentation master file, created by
   sphinx-quickstart on Sat Jun 18 19:05:57 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Witaj w dokumntacji lab3!
================================

.. toctree::
   :maxdepth: 2
   :caption: Zawartość:

   wprowadzenie
   instalacja
   architektura
   api



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
