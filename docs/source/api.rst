REST API
===============================

Otrzymanie informacji o wszystkich klientach w bazie
-------------------------------------------------------
Poniższa komenda zwraca wszystkich customerów w naszej bazie: id, imię i nazwisko:

.. code-block::

    GET localhost:8080/customers

Otrzymanie informacji dotyczących klienta o danym id
-------------------------------------------------------
Poniższa komenda zwraca informację na temat określonego customera:

.. code-block::

    GET localhost:8080/cutsomers/{id}