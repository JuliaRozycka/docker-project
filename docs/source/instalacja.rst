===============================================================
Instrukcja uruchamiania aplikacji za pomocą docker-compose
===============================================================
Wymagania wstępne
-------------------------------
Należy zainstalować:
- Docker 
- Docker compose

.. note::
   Oba narzędzia są dostępne w ramach instalacji Docker desktop

Zbudowanie aplikacji
----------------------------
Aplikację należy zbudować za pomocą IDE lub z wiersza poleceń:

.. code-block::

    gradle bootRun
    # lub
    ./gradlew build && java -jar build/libs/lab3-0.0.1-SNAPSHOT.jar

Budowanie aplikacji w kontenerze Docker
---------------------------------------------
- Należy skonfigurować plik Dockerfile używając szeregu instrukcji jak pokazano w kodzie poniżej


.. code-block::

    FROM gradle:6.0.1-jdk8 AS build
    USER root
    RUN mkdir app
    COPY src/ /app/src/
    COPY build.gradle /app/
    COPY settings.gradle /app/
    WORKDIR /app
    RUN gradle bootJarpsql -h localhost -p 5433 -U postgres


    FROM openjdk:8-jdk-alpine
    ARG JAR_FILE=build/libs/*.jar
    COPY ${JAR_FILE} app.jar
    EXPOSE 8080
    ENTRYPOINT ["java","-jar","/app.jar"]


- Następnie zbudować obraz za pomocą instrukcji:


.. code-block::

    docker build -t full-spring-boot .


- Uruchomić kontener:

.. code-block::

    docker run -p <docker_port>:<host_port> full-spring-boot

Jeśli wszystkie kroki zostaną wykonane poprawnie, aplikacja powinna się uruchomić w kontenerze co można zauważyć w Docker desktop.


Uruchomienie bazy danych w kontenerze
------------------------------------------

.. code-block::

    docker run --name test-db -p 5433:5432 -e POSTGRES_PASSWORD=userpassword -d postgres

Aby upewnić się iż obraz został utworzony można użyć komendy:
.. code-block::

    docker ps

- Następnie łączymy się z bazą danych za pomocą pgAdmin bądź z konsoli:

.. code-block::

   psql -h localhost -p 5433 -U postgres

Uruchomienie skryptów inicjalizującyh bazę danych
---------------------------------------------------

- Należy utworzyć plik o nazwie create-databases.sh będący skryptem inicjalizującym i umieścić go w katalogu docker-entrypoint-initdb.d.

.. code-block::

   #!/bin/bash
    set -e

    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
        CREATE USER docker;
        CREATE DATABASE docker;
        GRANT ALL PRIVILEGES ON DATABASE docker TO docker;
    EOSQL


- Następnie uruchomić kontener za pomocą poniższej komendy:

.. code-block::

    docker run --name test-db -p 5433:5432 -v <ścieżka-do-aplikacji>/docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d -e POSTGRES_PASSWORD=userpass -d postgres

Uruchomienie kliku kontenerów za pomocą Docker compose
-----------------------------------------------------------

- Do zależności w pliku build.gradle należy dodać

.. code-block::

    dependencies {
        implementation 'org.springframework.boot:spring-boot-starter-web'
        implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
        compile group: 'org.postgresql', name: 'postgresql', version: '42.2.9'
        testImplementation('org.springframework.boot:spring-boot-starter-test') {
                exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
    }

- Sprawdzić czy aplikacja działa poprawnie
- Następnie przetestować w przeglądarce następujące endpointy:

.. code-block::

    ### get individual customer
    GET http://localhost:8080/customers/{id} # id można odczytać z tabeli customer

    ### get all customers
    GET http://localhost:8080/customers


Uruchomienie bazy danych za pomocą Docker compose
------------------------------------------------------------

- Do katalogu nadrzędnego należy dodać plik docker-compose.yaml i dodać następującą zawartość:

.. code-block::

    version: "3"
    services:
        app:
            image: "app/customer"
            build:
                context: .
                dockerfile: "Dockerfile"
            environment:
                POSTGRES_USER: ${POSTGRES_USER}
                POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
            ports:
                - 8080:8080
        db:
            image: postgres:latest
            volumes:
                - "./docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d"
            environment:
                POSTGRES_USER: ${POSTGRES_USER}
                POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
            ports:
                - ${DB_PORT}:5432


- Następnie dodać plik .env i umieścić w nim następującą zawartość:

.. code-block::

    POSTGRES_USER=postgres
    POSTGRES_PASSWORD=mysecretpass
    DB_PORT=5433


- Konfiguracja dostępu do bazy w application-properties

.. code-block::

    server.port=8080

    ## Spring DATASOURCE (DataSourceAutoConfiguration & DataSourceProperties)
    spring.datasource.url=jdbc:postgresql://db:5432/docker
    spring.datasource.username=${POSTGRES_USER}
    spring.datasource.password=${POSTGRES_PASSWORD}

    # The SQL dialect makes Hibernate generate better SQL for the chosen database
    spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.PostgreSQLDialect

    # Hibernate ddl auto (create, create-drop, validate, update)
    spring.jpa.hibernate.ddl-auto = update

    spring.jpa.hibernate.show-sql=true


Kolejno należy zbudować obraz:

.. code-block::

    docker-compose build

A nastepnie uruchomić kontenery bazy danych i aplikacji za pomocą polecenia:

.. code-block::

    docker-compose up

Na koniec należy sprawdzić w przeglądarce czy wszystko działa poprawnie.

