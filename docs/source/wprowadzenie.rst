============================
Wprowadzenie
============================

Docker
----------------------------
Docker jest narzędziem, które służyć ma konteneryzacji aplikacji, uruchamianiu aplikacji w kontenerach.
Cała idea konteneryzacji opiera się na tym, że naszą aplikację możemy wsadzić w pewien wirtualny kontener raze
ze wszystkimi jej zależnościami, konfiguracjami, bibliotekami itp. - wszystkim, co jest niezbędne do uruchomienienia aplikacji
(jednak jest to rozwiązanie o wiele lżejsze niż np. wirtualizacja) dzięki czemu możemy bezproblemowo powielać nasza aplikację np. w celu
przeprowadzenia jakichś destrukcyjnych testów lub przeniesienia jej na inny komputer.
Diamterialnie skracają one czas pracy nad tworzeniem aplikacji. Zapewniają mobilnośc, lekkość i szybkość - dlatego warto je stosować.

Docker compose
----------------------------
Można powiedzieć, że docker compose jest o "krok dalej" niż jeden zwykły kontener. Jest to narzędzie, które pozwala
na tworzenie i uruchamianie wielu kontenerów na raz. W sytuacji, kiedy mamy jeden czy dwa kontenery to nie jest jakoś trudno
zadbać o to, aby się uruchamiały w odpowiednim momencie, w odpowiednej kolejności. Jeśli jednak mamy więcej tych kontenerów, moze to stwarzać problem.
Docker compose umożliwia nam zautomatyzowanie całego procesu, zdefiniowanie kolejności i warunków uruchomienia.