#Docker project

##About
It's a simple project, that was created for learn docker and docker-compose basics.
Furthermore, it was used to make full documentation.

###Docker
- Dockerfile can be found in main path.
- docker-entrypoint-initdb.d is used to run postgres database on container
- docker-compose.yml for managing two containers

###Documentation

Documentation can be found in _lab3/docs/build/html_.




